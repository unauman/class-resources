import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-drawer',
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.css']
})
export class DrawerComponent implements OnInit {

  repo_facts;
  nav: Array<any>;

  constructor(private data: DataService) { }

  ngOnInit() {
    this.data.semesters.subscribe(
      data => {
        this.nav = data;
      }
    )
    var footer = localStorage['footer'] || 'show';
    document.children[0].setAttribute('footer', footer);
  }

  format(num) {
    if (num > 1e4) {
      return (num / 1e3).toFixed(0) + "k";
    }
    else if (num > 1e3) {
      return (num / 1e3).toFixed(1) + "k";
    }
    else {
      return "" + num;
    }
  }


  toggleTheme() {
    if (document) {
      var theme = document.children[0].getAttribute('theme');
      var newTheme = theme == 'light' ? 'dark' : 'light';
      document.children[0].setAttribute('theme', newTheme);
      localStorage['theme'] = newTheme;
    }
  }

  toggleFooter(){
    var footer = document.children[0].getAttribute('footer');
    var status = footer == 'show' ? 'hide' : 'show';
    document.children[0].setAttribute('footer', status);
    localStorage['footer'] = status;
  }
}
