import { Component, OnInit } from '@angular/core';
import { DataService } from './../../services/data.service';
import { SearchService } from '../../services/search.service';
import { Router } from '@angular/router'
import { templateJitUrl } from '@angular/compiler';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  searchActive = false;
  fixed = false;
  searchResults = [];
  searchText = "";

  constructor(
    private search: SearchService,
    private router: Router
  ) { }

  ngOnInit() {
    if (window) {
      window.addEventListener('scroll', ()=>{
        var fixed = window.scrollY > 48;
        if(fixed != this.fixed){
          document.querySelector('header').classList.add('scrolled');
        }
        else if(document.querySelector('header').classList.contains('scrolled')){
          document.querySelector('header').classList.remove('scrolled');
        }
      })
    }
  }

  toggleDark() {
    if (document) {
      var theme = document.children[0].getAttribute('theme');
      var newTheme = theme == 'light' ? 'dark' : 'light';
      document.children[0].setAttribute('theme', newTheme);
      localStorage['theme'] = newTheme;
    }
  }

  keyDown(text){
    this.searchActive = true;
    this.searchResults = this.search.search(text);
  }

  getIcon(type){
    var icon = {
      "subject": "class",
      "links": 'link',
      "resource": 'save_alt'
    }
    return icon[type]
  }

  navSearch(item){
    if(item.type == 'subject'){
      this.router.navigateByUrl(`${item.sm}/${item.id}`)
    }
    else if (item.type == 'links' || item.type == 'resource'){
      window.open(item.url || item.download);
    }
    this.searchActive = false;
  }
}
