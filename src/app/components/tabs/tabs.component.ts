import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements OnInit {

  activeLevel = 0;
  semesters: Array<any>;

  scrolled = false;

  constructor(private router: Router, private data: DataService) { }

  ngOnInit() {
    if (window) {
      window.addEventListener('scroll', () => {
        this.scrolled = window.scrollY > 10;
      })
    }

    this.data.semesters.subscribe(
      data=>{
        this.semesters = data;
      }
    )
  }

}
