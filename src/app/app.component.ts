import { Component, OnInit } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  updateAvailable = false;

  constructor(private swUpdate: SwUpdate) { }

  ngOnInit(){
    this.swUpdate.available.subscribe(event => {
      this.updateAvailable = true;
    });
    this.swUpdate.checkForUpdate();
  }

  update(){
    location.reload();
  }
}
