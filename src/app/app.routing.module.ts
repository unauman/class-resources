import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubjectDetailComponent } from './pages/subject-detail/index.component';
import { SemesterDetailComponent } from './pages/semester-detail/semester-detail.component'
import { GpaCalcComponent } from './pages/gpa-calc/gpa-calc.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '1',
    pathMatch: 'full'
  }, {
    path: 'gpa-calc',
    component: GpaCalcComponent
  }, {
    path: ':semester',
    component: SemesterDetailComponent
  }, {
    path: ':semester/:subject',
    component: SubjectDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
