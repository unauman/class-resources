import { Component, Inject } from '@angular/core';
import { GpaService } from '../../services/gpa.service';

@Component({
  selector: 'app-gpa-calc',
  templateUrl: './gpa-calc.component.html',
  styleUrls: ['./gpa-calc.component.css']
})
export class GpaCalcComponent {

  constructor(private calculator: GpaService) {
  }

  marks = [
    {
      name: "Semester 1",
      percent: 0,
      gpa: 0,
      grade: "",
      expanded: true,
      subjects: [
        {
          name: "Applied Physics",
          creditHours: 2,
          percent: 0,
          gpa: 0,
          grade: "",
          expanded: true,
          quiz: [null, null],
          assign: [null, null],
          s1: [null, null],
          s2: [null, null],
          term: [null, null]
        }, {
          name: "Applied Physics (Lab)",
          creditHours: 2,
          percent: 0,
          gpa: 0,
          grade: "",
          expanded: true,
          quiz: [null, null],
          assign: [null, null],
          s1: [null, null],
          s2: [null, null],
          term: [null, null]
        }, {
          name: "Calculus",
          creditHours: 3,
          percent: 0,
          gpa: 0,
          grade: "",
          expanded: false,
          quiz: [null, null],
          assign: [null, null],
          s1: [null, null],
          s2: [null, null],
          term: [null, null]
        }
      ]
    }
  ]

  calcSemesterGPA() {
    for (var semester of this.marks) {
      for (var subject of semester.subjects) {

      }
    }
  }

  calc() {
    for (var semester of this.marks) {
      for (var subject of semester.subjects) {
        subject["percent"] = this.calculator.calcPerc(
          [subject.assign],
          [subject.quiz],
          subject.s1,
          subject.s2,
          subject.term
        ) * 100;
        [subject['grade'], subject['GPA']] = this.calculator.gradePoint(subject["percent"] / 100);
      }

      [semester['percent'], semester['grade'], semester['GPA']] = this.calculator.calcSemesterGPA(semester);
    }
  }

  addSmester() {
    this.marks.push({
      name: `Semester ${this.marks.length + 1}`,
      percent: 0,
      gpa: 0,
      grade: "",
      expanded: false,
      subjects: []
    })
  }

  addSubject(sm) {
    var sms: any = this.marks.filter((el) => el.name == sm.name);
    console.log(sms);
    sms[0].subjects.push({
      name: "Untitled Subject",
      creditHours: 4,
      percent: 0,
      gpa: 0,
      grade: "",
      expanded: true,
      quiz: [null, null],
      assign: [null, null],
      s1: [null, null],
      s2: [null, null],
      term: [null, null]
    })
  }
}