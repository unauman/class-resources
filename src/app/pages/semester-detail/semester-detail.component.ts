import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { DataService } from '../../services/data.service'

@Component({
  selector: 'app-semester-detail',
  templateUrl: './semester-detail.component.html',
  styleUrls: ['./semester-detail.component.css']
})
export class SemesterDetailComponent implements OnInit {

  semester = { 'name': '', 'subjects': [], 'links': [] };
  isLoading = true;
  error = null;

  constructor(private data: DataService, private router: ActivatedRoute) { }

  ngOnInit() {
    this.router.params.subscribe(params => {
      var id = params['semester'];
      var semester = this.data.getSemester(id);
      var subjects = this.data.getSubjects(id);
      var links = this.data.getLinks(id);

      semester.subscribe(
        data => {
          this.semester['name'] = data['name'];
          this.semester['id'] = data['id'];
          this.isLoading = false;
        },
        err => {
          this.error = err;
        }
      )

      subjects.subscribe(
        data => {
          this.semester['subjects'] = data;
        },
        err => {
          this.error = err;
        }
      )

      links.subscribe(
        data => {
          this.semester['links'] = data;
        },
        err =>{ 
          this.error = err;
        }
      )
    })

  }

}