import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { DataService } from '../../services/data.service'
import { CcwScrapper } from '../../services/ccwScraper.services'

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class SubjectDetailComponent implements OnInit {

    subject = { "name": "Loading...", "resources": [] }
    scrappedResources = [];
    isLoading = true;
    loadingText = "Fetching...";
    error = null;

    constructor(
        private data: DataService,
        private router: ActivatedRoute,
        private courseScrapper: CcwScrapper
    ) { }

    ngOnInit() {
        this.router.params.subscribe(params => {
            var sm = params['semester'];
            var subj = params['subject'];
            var subject = this.data.getSubject(sm, subj);

            subject.subscribe(data => {
                this.subject['name'] = data['name'];
                this.subject['id'] = data['id'];
                this.subject['isHybrid'] = data['isHybrid'];

                if (data['isHybrid']) {
                    this.courseScrapper.setLogger(this.logger());
                    var course = this.courseScrapper.scrapCourse(data['id']);
                    course.then(data => {
                        this.subject['resources'] = data.lectures;
                        this.subject['lastUpdate'] = data.lastUpdate;
                        this.isLoading = false;
                    })
                }
                else {
                    var links = this.data.getResources(sm, subj);
                    links.subscribe(data => {
                        this.subject['resources'] = data;
                        this.isLoading = false;
                    })
                }

            })

        })
    }

    refetchAll() {
        this.isLoading = true;
        localStorage['courses'] = '';
        var course = this.courseScrapper.scrapCourse(this.subject['id'], true);
        course.then(data => {
            this.subject['resources'] = data.lectures;
            this.subject['lastUpdate'] = data.lastUpdate;
            this.isLoading = false;
        })
    }

    logger() {
        return {
            'log': (text) => {
                this.loadingText = text;
            }
        }
    }

    getIcon(type) {
        var types = ['doc', 'ppt', 'pdf', 'img', 'zip']
        return `../../../assets/file-icons/${types.indexOf(type) > -1 ? type : 'file'}.svg`;
    }
}