import { Injectable } from '@angular/core';
import * as Fuse from 'fuse.js';
import { DataService } from './data.service';

@Injectable({
    providedIn: 'root'
})
export class SearchService {

    searchIndex = [];
    private loaded = false;
    private options = {
        shouldSort: true,
        threshold: 0.4,
        location: 0,
        distance: 100,
        maxPatternLength: 32,
        minMatchCharLength: 1,
        keys: ["name", "type"]
    }
    private fuse;

    constructor(private data: DataService) {
        if (!this.loaded) this.updateSemesters();
    }

    private updateSemesters() {
        this.data.getSemesters().subscribe(data => {
            data.map(res => {
                this.updateSubjects(res.id);;
            })
        })
    }

    private updateSubjects(sm) {
        this.data.getSubjects(sm).subscribe(data => {
            data.map(res => {
                this.pushIndex({...res, sm: sm}, 'subject')
                this.updateResources(sm, res.id);
            })
        })

        this.data.getLinks(sm).subscribe(data => {
            data.map(res => this.pushIndex(res, 'links'))
        })
    }

    private updateResources(sm, sub) {
        this.data.getResources(sm, sub).subscribe(data => {
            data.map(res => {
                if(!res.isHeading)
                    this.pushIndex(res, 'resource')
            })
            this.loaded = true;
            this.fuse = new Fuse(this.searchIndex, this.options);
        })
    }

    private pushIndex(data, type) {
        // Check if value already exists
        var sameItems = this.searchIndex.filter(val => val.id == data.id);

        if (sameItems.length == 0) {
            this.searchIndex.push({
                type: type,
                ...data
            })
        }
    }

    search(text) {
        if(this.fuse){
            return this.fuse.search(text);
        }
        return [];
    }

}