import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import * as cheerio from 'cheerio';
// import * as rp from 'rp';
import { map } from 'rxjs/operators'

@Injectable({
    providedIn: 'root'
})
export class IctScrapperService {

    $: any;
    data: Array<any>;

    constructor(private http: HttpClient) { }

    async scrap(url) {
        return new Promise(resolve => {
            this.http.get(`https://cors-anywhere.herokuapp.com/${url}`, { responseType: 'text' })
                .subscribe(data => {
                    resolve(this.scrapFiles_(data));
                });
        })
    }

    scrapFiles_(html) {
        var parser = new DOMParser();
        var dom = parser.parseFromString(html, 'text/html');
        this.data = [];
        console.log(dom);

        var files = dom.querySelectorAll('td.td-file');
        console.log(files);

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var fileData = this.scrapFileData_(file);
            if(fileData.text){
                this.data.push(fileData);
            }
        }

        return this.data;
    }

    scrapFileData_(file) {
        var text = file.querySelector('span:first-child').innerText;
        var links = file.querySelectorAll('span>a')
        var actions = []
        for(var link of links){
            actions.push({
                text: link.innerText,
                url: link.getAttribute('href')
            })
        }
             

        return {
            text: text,
            links: actions
        }
    }
}
