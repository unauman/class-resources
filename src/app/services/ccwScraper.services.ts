import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CcwScrapper {

    proxyServer = 'https://cors-anywhere.herokuapp.com/';
    baseUrl = 'http://ccw.vcomsats.edu.pk';
    courseUrl = `${this.baseUrl}/Course.aspx?CID`;
    lessonUrl = `${this.baseUrl}/Lesson.aspx?LID`
    logger = console;

    localCourses;
    CourseLastUpdate;

    constructor() {
        this.localCourses = JSON.parse(localStorage['courses'] || '{}');
        this.CourseLastUpdate = JSON.parse(localStorage['courseLastUpdate'] || '{}');
    }

    updatelocalStorage() {
        localStorage['courses'] = JSON.stringify(this.localCourses);
        localStorage['courseLastUpdate'] = JSON.stringify(this.CourseLastUpdate);
    }

    setLogger(logger) {
        this.logger = logger;
    }

    /**
     * Fetch document from url
     * @param {string} url 
     */
    async fetchUrl(url): Promise<string> {
        // proxyServer can be a function that takes url and return proxy url
        url = this.proxyServer + url;

        return new Promise<string>((resolve) => {
            var oReq = new XMLHttpRequest();

            var reqListener = () => {
                resolve(oReq.responseText);
            }

            oReq.addEventListener("load", reqListener);
            oReq.open("GET", url);
            oReq.setRequestHeader('x-requested-with', this.baseUrl)
            oReq.send();
        })

    }

    getLocalLecture(courseId, id) {
        if (this.localCourses[courseId])
            return this.localCourses[courseId].find(e => e.id = id)
        return null
    }

    async scrapCourse(courseId, force = false) {
        var lectures;
        if (this.localCourses[courseId]) {
            this.logger.log(`Found "${this.localCourses[courseId].length} Lecture(s). Fetching Remaining..."`);
        }
        try {
            lectures = await this.getLessons(courseId);

            for (var lecture of lectures) {
                var localLecture = this.getLocalLecture(courseId, lecture.id);

                if (localLecture && !force) {
                    lecture['resources'] = localLecture['resources'];
                }
                else {
                    this.logger.log(`Fetching "${lecture.name}"`);
                    lecture['resources'] = await this.getLessonResources(lecture.id);
                }
            }

            this.localCourses[courseId] = lectures;
            this.CourseLastUpdate[courseId] = new Date();
            this.updatelocalStorage();
        } catch (e) {
            lectures = this.localCourses[courseId];
        }

        return {lectures: lectures, lastUpdate: this.CourseLastUpdate[courseId]};
    }

    /**
     * Scrap course with all its resources
     * @param {string} courseId base64 encoded courseid
     */
    async getLessons(courseId) {
        // fetch course page
        var html = await this.fetchUrl(`${this.courseUrl}=${courseId}==`);

        // html Parser
        var parser = new DOMParser();
        var dom = parser.parseFromString(html, 'text/html');

        var links = dom.querySelectorAll('.course-lession-list>li>a');
        var lectures = [];

        for (var i = 0; i < links.length; i++) {
            var link: any = links[i];
            lectures.push({
                id: link.getAttribute('href').slice(16, -2),
                name: link.querySelector('div>div:first-child').innerText.trim(' ').trim('-'),
            })
        }

        return lectures;
    }

    /**
     * Fetch reources of given lesson
     * @param {string} id base64 encoded Lesson ID
     */
    async getLessonResources(id) {
        var html = await this.fetchUrl(`${this.lessonUrl}=${id}==`);
        var parser = new DOMParser();
        var dom = parser.parseFromString(html, 'text/html');
        var links = dom.querySelectorAll('.course-lession-item tr>td:nth-child(2)>a');
        var resources = [];

        for (var i = 0; i < links.length; i++) {
            var link: any = links[i];
            resources.push({
                title: link.innerText.trim(' '),
                url: link.getAttribute('href')
            })
        }

        return resources
    }
}
