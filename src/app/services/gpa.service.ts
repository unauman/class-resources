import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GpaService {

  constructor() { }
  s1Weight = 10;
  s2Weight = 15;
  finalWeight = 50;
  quizWeight = 15;
  assignWeight = 10;

  calcTotal(entity) {
    return entity.reduce(
      (totalMarks, crntMarks) =>
        [
          parseInt(totalMarks[0]) + parseInt(crntMarks[0]),
          parseInt(totalMarks[1]) + parseInt(crntMarks[1])
        ]
    )
  }

  percent(marks) {
    if (marks[1] <= 0) return 0;
    return marks[0] / marks[1];
  }

  getWeight(weight, entity) {
    return (entity[1] > 0) ? weight : 0;
  }

  calcPerc(assignments, quizzes, s1, s2, final) {
    var assignMarks = this.calcTotal(assignments)
    var quizMarks = this.calcTotal(quizzes);
    var asignPercent = this.percent(assignMarks);
    var quizzPercent = this.percent(quizMarks);
    var s1Percent = this.percent(s1)
    var s2Percent = this.percent(s2)
    var finalPercent = this.percent(final)

    return Math.round((
      (asignPercent ? asignPercent * this.assignWeight : 0) +
      (quizzPercent ? quizzPercent * this.quizWeight : 0) +
      (s1Percent ? s1Percent * this.s1Weight : 0) +
      (s2Percent ? s2Percent * this.s2Weight : 0) +
      (finalPercent ? finalPercent * this.finalWeight : 0)
    ) / (
        this.getWeight(this.assignWeight, assignMarks) +
        this.getWeight(this.quizWeight, quizMarks) +
        this.getWeight(this.s1Weight, s1) +
        this.getWeight(this.s2Weight, s2) +
        this.getWeight(this.finalWeight, final)
      ) * 100) / 100;
  }

  gradePoint(percent): [string, number] {
    var gradePoints = [
      ['A', 0.9],
      ['A-', 0.85],
      ['B+', 0.8],
      ['B', 0.75],
      ['B-', 0.7],
      ['C+', 0.65],
      ['C', 0.6],
      ['C-', 0.55],
      ['D', 0.5]
    ]
    for (var point of gradePoints) {
      if (percent >= point[1])
        return [point[0].toString(), Math.round(percent * 400) / 100]
    }
    return ['F', Math.round(percent * 400) / 100];
  }

  calcSemesterGPA(semester): [number, string, number] {
    var percent = 0, ch = 0;
    for (var subject of semester.subjects) {
      percent += subject.percent ? subject.percent * subject.creditHours : 0;
      ch += subject.percent ? subject.creditHours : 0;
    }

    percent /= ch;

    var gradePoint = this.gradePoint(percent / 100);
    return [percent, gradePoint[0], gradePoint[1]]
  }
}
