import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component'
import { TabsComponent } from './components/tabs/tabs.component';
import { DrawerComponent } from './components/drawer/drawer.component';
import { MatExpansionModule } from '@angular/material';

import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireModule } from 'angularfire2';

import { JumboComponent } from './pages/home/components/jumbo/jumbo.component';
import { TestSectionComponent } from './pages/home/components/test-section/test-section.component';
import { HomeComponent } from './pages/home/home.component';

import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component';
import { ErrorComponent } from './components/error/error.component';

import { SubjectDetailComponent } from './pages/subject-detail/index.component';
import { SemesterDetailComponent } from './pages/semester-detail/semester-detail.component'

import { AppRoutingModule } from './app.routing.module';
import { environment } from '../environments/environment';
import { DataService } from './services/data.service';
import { SearchService } from './services/search.service';
import { IctScrapperService } from './services/ictScrapper.services';
import { ServiceWorkerModule } from '@angular/service-worker';
import { GpaService } from './services/gpa.service';
import { GpaCalcComponent } from './pages/gpa-calc/gpa-calc.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TabsComponent,
    FooterComponent,
    JumboComponent,
    TestSectionComponent,
    HomeComponent,
    SubjectDetailComponent,
    SemesterDetailComponent,
    DrawerComponent,
    LoadingSpinnerComponent,
    ErrorComponent,
    GpaCalcComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'class-resources'}),
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule.enablePersistence(),
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    BrowserAnimationsModule,
    MatExpansionModule
  ],
  providers: [
    DataService,
    SearchService,
    IctScrapperService,
    GpaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
