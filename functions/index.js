require('zone.js/dist/zone-node');

const functions = require('firebase-functions');
var compression = require('compression');
var minify = require('html-minifier').minify;
const express = require('express');
const path = require('path');
const { enableProdMode } = require('@angular/core');
const { renderModuleFactory } = require('@angular/platform-server');

const { AppServerModuleNgFactory } = require('./dist/server/main');

enableProdMode();

const index = require('fs')
  .readFileSync(path.resolve(__dirname, './dist/browser/index.html'), 'utf8')
  .toString();

let app = express();

app.use(express.static('./dist/browser/'))

app.use(compression());

app.get('**', function (req, res) {
  console.log("REQUEST:", req.originalUrl);
  renderModuleFactory(AppServerModuleNgFactory, {
    url: req.path,
    document: index
  }).then((html) => {
    return res.status(200).send(minify(html, {
      minifyCSS: true,
      minifyJS: true,
      collapseWhitespace: true
    }))
  });
});

// app.listen(4100);

exports.ssr = functions.https.onRequest(app);