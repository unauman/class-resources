# Class Resources

An angular app, providing a platform for sharing class resources. Mainly developed for FA18-BCS-C Comsats Lahore.

## Running Locally

```bash
# Cloning repo
git clone https://github.com/nmanumr/class-resources --depth=1
cd class-resources

# Install dependencies using npm
npm install

# Or using yarn
yarn

# Running dev server (assumed angular-cli is installed globally)
ng serve

# Build app (/docs)
ng build

# deploy to firebase (assuming firebase project initialized properly)
firebase deploy
```